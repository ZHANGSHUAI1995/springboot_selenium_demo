package com.example.springboot_selenium_demo.pojo;

import java.util.Date;

public class Country {
    private Integer countryId;

    private Integer amazonRegionId;

    private Integer amazonInventoryRegionId;

    private String countryName;

    private String alphaCode;

    private String countryUrl;

    private String capital;

    private String vat;

    private String marketplace;

    private Integer currencyId;

    private Date utime;

    private String sellercentralDateFormat;

    public Country(Integer countryId, Integer amazonRegionId, Integer amazonInventoryRegionId, String countryName, String alphaCode, String countryUrl, String capital, String vat, String marketplace, Integer currencyId, Date utime, String sellercentralDateFormat) {
        this.countryId = countryId;
        this.amazonRegionId = amazonRegionId;
        this.amazonInventoryRegionId = amazonInventoryRegionId;
        this.countryName = countryName;
        this.alphaCode = alphaCode;
        this.countryUrl = countryUrl;
        this.capital = capital;
        this.vat = vat;
        this.marketplace = marketplace;
        this.currencyId = currencyId;
        this.utime = utime;
        this.sellercentralDateFormat = sellercentralDateFormat;
    }

    public Country() {
        super();
    }

    public Integer getCountryId() {
        return countryId;
    }

    public void setCountryId(Integer countryId) {
        this.countryId = countryId;
    }

    public Integer getAmazonRegionId() {
        return amazonRegionId;
    }

    public void setAmazonRegionId(Integer amazonRegionId) {
        this.amazonRegionId = amazonRegionId;
    }

    public Integer getAmazonInventoryRegionId() {
        return amazonInventoryRegionId;
    }

    public void setAmazonInventoryRegionId(Integer amazonInventoryRegionId) {
        this.amazonInventoryRegionId = amazonInventoryRegionId;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName == null ? null : countryName.trim();
    }

    public String getAlphaCode() {
        return alphaCode;
    }

    public void setAlphaCode(String alphaCode) {
        this.alphaCode = alphaCode == null ? null : alphaCode.trim();
    }

    public String getCountryUrl() {
        return countryUrl;
    }

    public void setCountryUrl(String countryUrl) {
        this.countryUrl = countryUrl == null ? null : countryUrl.trim();
    }

    public String getCapital() {
        return capital;
    }

    public void setCapital(String capital) {
        this.capital = capital == null ? null : capital.trim();
    }

    public String getVat() {
        return vat;
    }

    public void setVat(String vat) {
        this.vat = vat == null ? null : vat.trim();
    }

    public String getMarketplace() {
        return marketplace;
    }

    public void setMarketplace(String marketplace) {
        this.marketplace = marketplace == null ? null : marketplace.trim();
    }

    public Integer getCurrencyId() {
        return currencyId;
    }

    public void setCurrencyId(Integer currencyId) {
        this.currencyId = currencyId;
    }

    public Date getUtime() {
        return utime;
    }

    public void setUtime(Date utime) {
        this.utime = utime;
    }

    public String getSellercentralDateFormat() {
        return sellercentralDateFormat;
    }

    public void setSellercentralDateFormat(String sellercentralDateFormat) {
        this.sellercentralDateFormat = sellercentralDateFormat == null ? null : sellercentralDateFormat.trim();
    }
}