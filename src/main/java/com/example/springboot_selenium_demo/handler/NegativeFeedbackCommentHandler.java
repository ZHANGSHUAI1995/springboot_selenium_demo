package com.example.springboot_selenium_demo.handler;

import com.example.springboot_selenium_demo.service.TestService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

/**
 * @author peter
 * @title: NegativeFeedbackCommentHandler
 * @projectName springboot_selenium_demo
 * @description: TODO
 * @date 19-8-2下午3:00
 */
@Controller
@RequestMapping(value = "/comment")
public class NegativeFeedbackCommentHandler {

    @Autowired
    private TestService testService;


    @ApiOperation(value = "登录测试，获取cookie")
    @RequestMapping(value = "/test1", method = RequestMethod.GET)
    @ResponseBody
    public String test() {
        System.out.println("test1");

        testService.getCookie();


        return "success";
    }

    @ApiOperation(value = "测试comment逻辑")
    @RequestMapping(value = "/testComment", method = RequestMethod.GET)
    @ResponseBody
    public String testComment() {
        System.out.println("testComment");

        testService.testComment();

        return "success";
    }


}
