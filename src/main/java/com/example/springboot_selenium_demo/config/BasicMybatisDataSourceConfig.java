package com.example.springboot_selenium_demo.config;

import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;

import javax.sql.DataSource;

@Configuration
@MapperScan(basePackages = {"com.example.springboot_selenium_demo.mapper"}, sqlSessionFactoryRef = "basicSqlSessionFactory")
public class BasicMybatisDataSourceConfig {

    // 精确到master目录和其他数据源隔离/media/peter/Data/dev/bisonSpace/springboot_selenium_demo/src/main/java/com/example/springboot_selenium_demo
    static final String MAPPER_LOCATION = "classpath:com/example/springboot_selenium_demo/mapper/*.xml";

    @Bean(name = "basicSqlSessionFactory")
    @Primary
    public SqlSessionFactory masterSqlSessionFactory(@Qualifier("basic") DataSource masterDataSource)
            throws Exception {
        final SqlSessionFactoryBean sessionFactory = new SqlSessionFactoryBean();
        sessionFactory.setDataSource(masterDataSource);
        sessionFactory.setMapperLocations(new PathMatchingResourcePatternResolver()
                .getResources(BasicMybatisDataSourceConfig.MAPPER_LOCATION));
        return sessionFactory.getObject();
    }
}
