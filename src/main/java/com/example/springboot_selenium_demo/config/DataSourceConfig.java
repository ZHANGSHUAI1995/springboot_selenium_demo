package com.example.springboot_selenium_demo.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import javax.sql.DataSource;

@Configuration
public class DataSourceConfig {
    @Bean("basic")
    @Primary
    @ConfigurationProperties(prefix = "spring.datasource.basic")
    public DataSource dataSource() {
        return DataSourceBuilder.create().build();
    }

/*    @Bean("mail")
    @ConfigurationProperties(prefix = "spring.datasource.mail")
    public DataSource mailDataSource() {
        return DataSourceBuilder.create().build();
    }

    @Bean("order")
    @ConfigurationProperties(prefix = "spring.datasource.order")
    public DataSource orderDataSource() {
        return DataSourceBuilder.create().build();
    }*/
}