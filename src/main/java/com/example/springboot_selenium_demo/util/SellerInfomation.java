package com.example.springboot_selenium_demo.util;



public class SellerInfomation {
	public String getLoginName() {
		return loginName;
	}
	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}
	public String getPassword(String privateKey) {
		byte[] decryptByPrivateKey = RSAUtils.decryptByPrivateKey(Base64Utils.decode(password), privateKey);
		return new String(decryptByPrivateKey);
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getTwoStepCode(String privateKey) {
		byte[] decryptByPrivateKey = RSAUtils.decryptByPrivateKey(Base64Utils.decode(twoStepCode), privateKey);
		return new String(decryptByPrivateKey);
	}
	public void setTwoStepCode(String twoStepCode) {
		this.twoStepCode = twoStepCode;
	}
	public String getSellerServer() {
		return sellerServer;
	}
	public void setSellerServer(String sellerServer) {
		this.sellerServer = sellerServer;
	}
	public int getSellerServerPort() {
		return sellerServerPort;
	}
	public void setSellerServerPort(int sellerServerPort) {
		this.sellerServerPort = sellerServerPort;
	}
	private String loginName;
	private String password;
	private String twoStepCode;
	private String sellerServer;
	private int sellerServerPort;
}
