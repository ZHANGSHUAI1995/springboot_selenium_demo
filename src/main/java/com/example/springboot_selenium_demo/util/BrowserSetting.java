package com.example.springboot_selenium_demo.util;

public class BrowserSetting {
	private String server;
	private int port;
	private boolean headless=false;

	
	private boolean captureScreen=false;
	private String captureScreenPath="/tmp/FirefoxCapture";
	private String remoteBrowserDownloadPath="/tmp/FirefoxDownload";
	
	public boolean isCaptureScreen() {
		return captureScreen;
	}
	public void setCaptureScreen(boolean captureScreen) {
		this.captureScreen = captureScreen;
	}
	public String getCaptureScreenPath() {
		return captureScreenPath;
	}
	public void setCaptureScreenPath(String captureScreenPath) {
		this.captureScreenPath = captureScreenPath;
	}
	public String getServer() {
		return server;
	}
	public void setServer(String server) {
		this.server = server;
	}
	public int getPort() {
		return port;
	}
	public void setPort(int port) {
		this.port = port;
	}
	
	public String getRemoteBrowserDownloadPath() {
		return remoteBrowserDownloadPath;
	}
	public void setRemoteBrowserDownloadPath(String remoteBrowserDownloadPath) {
		this.remoteBrowserDownloadPath = remoteBrowserDownloadPath;
	}
	public boolean isHeadless() {
		return headless;
	}
	public void setHeadless(boolean isHeadless) {
		this.headless = isHeadless;
	}
}
