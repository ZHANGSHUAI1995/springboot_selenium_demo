package com.example.springboot_selenium_demo.util;

public enum EPlatform {
    Digital_Unix, FreeBSD, HP_UX, Irix, Linux, Mac_OS, Mac_OS_X, MPEiX, NetWare_411, OpenVMS, OS2, OS390, OSF1, Solaris, SunOS, Windows, Others, AIX
}
