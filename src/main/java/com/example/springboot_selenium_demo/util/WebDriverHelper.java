package com.example.springboot_selenium_demo.util;

import org.openqa.selenium.WebDriver;

import java.net.URL;

public class WebDriverHelper {
	public static String getHostAndPort(WebDriver driver) {
		StringBuilder urlStringBuilder=new StringBuilder();
		try {
			URL url = new URL(driver.getCurrentUrl());
		    urlStringBuilder.append(url.getProtocol());
		    urlStringBuilder.append("://");
		    urlStringBuilder.append(url.getHost());
		    if(url.getPort()!=-1) {
		    	urlStringBuilder.append(":");
		    	urlStringBuilder.append(url.getPort());
		    }	
		} catch (Exception e) {
		}
		return urlStringBuilder.toString();
	}
}
