package com.example.springboot_selenium_demo.util;

import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import ru.yandex.qatools.ashot.AShot;

import javax.imageio.ImageIO;
import java.io.File;

public abstract class MarketplaceHandler {
	
	protected RemoteWebDriver driver;
	protected DesiredCapabilities desiredCapabilities;
	protected BrowserSetting browserSetting;
	protected String browserName;
	protected AShot aShot;
	
	public RemoteWebDriver getDriver() {
		return driver;
	}

	public void setDriver(RemoteWebDriver driver) {
		this.driver = driver;
	}

	public DesiredCapabilities getDesiredCapabilities() {
		return desiredCapabilities;
	}

	public void setDesiredCapabilities(DesiredCapabilities desiredCapabilities) {
		this.desiredCapabilities = desiredCapabilities;
	}

	public BrowserSetting getBrowserSetting() {
		return browserSetting;
	}

	public void setBrowserSetting(BrowserSetting browserSetting) {
		this.browserSetting = browserSetting;
	}

	public String getBrowserName() {
		return browserName;
	}

	public void setBrowserName(String browserName) {
		this.browserName = browserName;
	}

	public AShot getaShot() {
		return aShot;
	}

	public void setaShot(AShot aShot) {
		this.aShot = aShot;
	}

	public abstract void jobInMarketplace(String marketPlace);
	
	public void shotCapture(String fileName) {
		try {
			if(browserSetting.isCaptureScreen()) {
				String filePath=String.format("%s/%s.png", browserSetting.getCaptureScreenPath(), fileName);
				ImageIO.write(aShot.takeScreenshot(driver).getImage(),"PNG",new File(filePath));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}
