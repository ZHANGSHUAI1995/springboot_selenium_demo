package com.example.springboot_selenium_demo.util;

import org.openqa.selenium.remote.SessionId;

public class ActiveHttpCommandSession {
	SessionId sessionId;
	public SessionId getSessionId() {
		return sessionId;
	}
	public void setSessionId(SessionId sessionId) {
		this.sessionId = sessionId;
	}
	public ActiveHttpCommandExecutor getActiveHttpCommandExecutor() {
		return activeHttpCommandExecutor;
	}
	public void setActiveHttpCommandExecutor(ActiveHttpCommandExecutor activeHttpCommandExecutor) {
		this.activeHttpCommandExecutor = activeHttpCommandExecutor;
	}
	ActiveHttpCommandExecutor activeHttpCommandExecutor;

	public ActiveHttpCommandSession(SessionId sessionId,
	ActiveHttpCommandExecutor activeHttpCommandExecutor) {
		this.sessionId=sessionId;
		this.activeHttpCommandExecutor=activeHttpCommandExecutor;
	}
}
