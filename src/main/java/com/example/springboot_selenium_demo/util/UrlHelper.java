package com.example.springboot_selenium_demo.util;

import java.util.*;

public class UrlHelper {
	public static List<String> splitUrlParam(String url)
	{
		List<String> result=null;
		int startIndex=url.indexOf("?");
		if(startIndex>=0)
		{
			url=url.substring(startIndex+1, url.length());
		}
		String[] param=url.split("&");
		result=Arrays.asList(param);
		return result;
	}
	
	public static Hashtable<String, String> splitParam(String url)
	{
		Hashtable<String, String> result=new Hashtable<String, String>();
		int startIndex=url.indexOf("?");
		if(startIndex>=0)
		{
			url=url.substring(startIndex+1, url.length());
		}
		String[] params=url.split("&");
		for (int i = 0; i < params.length; i++) {
			String param=params[i];
			String[] temp=param.split("=");
			if(temp.length>1)
			{
				result.put(temp[0], temp[1]);
			}else{
				result.put(temp[0], "");
			}
		}
		return result;
	}
	
	public static String createAjaxGetScript(String ajaxURL) {
		String ajaxString="var callback=arguments[arguments.length-1];"
				+ "$.ajax("
				+ "{"
				+ "type: \"GET\","
				+ "url: \""+ajaxURL+"\","
				+ "success: function(data) {if(typeof data=='object'){callback(JSON.stringify(data));}else{callback(data);}},"
				+ "error:function(response, textStatus, errorThrown){callback(\"failed\")}"
				+ "}"
				+ ");";
		return ajaxString;
	}
	
	public static String createAjaxOriginalGetScript(String ajaxURL, HashMap<String, String> headers) {
		
		String ajaxString="var callback=arguments[arguments.length-1];"
				+ "var ajax = new XMLHttpRequest();"
				+"ajax.open('get',\""+ajaxURL+"\", false);";		
		Set<String> keySet = headers.keySet();
		for (String string : keySet) {
			ajaxString=ajaxString+ "ajax.setRequestHeader(\""+string+"\",\""+headers.get(string)+"\");";		
		}
		
		ajaxString=ajaxString+  "ajax.onreadystatechange = function () {"
				+ "if (ajax.readyState==4 &&ajax.status==200) {"
				+ "if(typeof ajax.responseText=='object'){callback(JSON.stringify(ajax.responseText));}else{callback(ajax.responseText);};"
				+ "}else{"
				+ "callback(\"failed\");"
				+ "}"
				+ "};"
				+ "ajax.send();";
		return ajaxString;
	}
	
	public static String createAjaxOriginalPostScript(String ajaxURL, HashMap<String, String> headers, boolean isJSON, String param) {
		String ajaxString="var callback=arguments[arguments.length-1];"
				+ "var ajax = new XMLHttpRequest();"
				+"ajax.open('post',\""+ajaxURL+"\", false);";		
		Set<String> keySet = headers.keySet();
		for (String string : keySet) {
			ajaxString=ajaxString+ "ajax.setRequestHeader(\""+string+"\",\""+headers.get(string)+"\");";		
		}
		if (param!=null) {
			ajaxString=ajaxString+"var data;";
			if(isJSON) {
				ajaxString=ajaxString+"data=JSON.stringify("+param+");";
				ajaxString=ajaxString+"ajax.setRequestHeader(\"Content-Type\", \"application/json\");";
			}else {
				ajaxString=ajaxString+"data="+param+";";	
			}
		}		
		ajaxString=ajaxString+  "ajax.onreadystatechange = function () {"
				+ "if (ajax.readyState==4 && (ajax.status==200 || ajax.status==201)) {"
				+ "if(typeof ajax.responseText=='object'){callback(JSON.stringify(ajax.responseText));}else{callback(ajax.responseText);};"
				+ "}else{"
				+ "callback(\"failed\");"
				+ "}"
				+ "};";
		if (param!=null) {
			ajaxString=ajaxString+ "ajax.send(data);";
		}else {
			ajaxString=ajaxString+ "ajax.send();";
		}
				
		return ajaxString;
	}
}
