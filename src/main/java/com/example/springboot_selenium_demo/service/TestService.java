package com.example.springboot_selenium_demo.service;

/**
 * @author peter
 * @title: TestService
 * @projectName springboot_selenium_demo
 * @description: TODO
 * @date 19-8-2下午3:27
 */
public interface TestService {
    void getCookie();

    void testComment();
}
