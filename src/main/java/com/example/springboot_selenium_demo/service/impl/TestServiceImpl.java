package com.example.springboot_selenium_demo.service.impl;

import com.example.springboot_selenium_demo.service.TestService;
import com.example.springboot_selenium_demo.util.OSinfo;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.cookie.BasicClientCookie;
import org.openqa.selenium.By;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.springframework.stereotype.Service;

import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * @author peter
 * @title: TestServiceImpl
 * @projectName springboot_selenium_demo
 * @description: TODO
 * @date 19-8-2下午3:27
 */
@Service
public class TestServiceImpl implements TestService {

    WebDriver driver;

    @Override
    public void getCookie() {

        //判断当前运行系统
        if (OSinfo.isLinux()){
            //如果是Linux系统
            //初始化参数
            //System.setProperty("webdriver.firefox.marionette", "/media/peter/Data/dev/bisonSpace/springboot_selenium_demo/src/main/resources/static/geckodriver-v0.24.0-linux64/geckodriver");
            //FirefoxDriver.SystemProperty systemProperty = new FirefoxDriver.SystemProperty();
            System.setProperty("webdriver.gecko.driver", "/media/peter/Data/dev/bisonSpace/springboot_selenium_demo/src/main/resources/static/geckodriver-v0.24.0-linux64/geckodriver");
            driver = new FirefoxDriver();
        }else if (OSinfo.isWindows()){
            System.setProperty("webdriver.chrome.driver", "D:/F/workspace/ideaproject/springboot_selenium_demo/src/main/resources/static/chromedriver_win32/chromedriver.exe");
            driver = new ChromeDriver();
        }



        //driver = getDriver();
        //String baseUrl = "https://www.amazon.com/ap/signin?openid.pape.max_auth_age=0&openid.return_to=https%3A%2F%2Fwww.amazon.com%2F%3Fref_%3Dnav_ya_signin&openid.identity=http%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0%2Fidentifier_select&openid.assoc_handle=usflex&openid.mode=checkid_setup&openid.claimed_id=http%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0%2Fidentifier_select&openid.ns=http%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0&";
        String baseUrl = "https://www.amazon.com/ap/signin?openid.pape.max_auth_age=0&openid.return_to=https%3A%2F%2Fwww.amazon.com%2F%3Fref_%3Dnav_ya_signin&openid.identity=http%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0%2Fidentifier_select&openid.assoc_handle=usflex&openid.mode=checkid_setup&openid.claimed_id=http%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0%2Fidentifier_select&openid.ns=http%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0&";
        //加载url
        this.driver.get(baseUrl);
        //等待加载完成
        this.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

        // 获取页面元素
        WebElement elemEmail = this.driver.findElement(By.name("email"));
        WebElement elemPassword = this.driver.findElement(By.name("password"));
        // WebElement btn = driver.findElement(By.className("logging"));
        WebElement btn = this.driver.findElement(By.id("signInSubmit"));
        WebElement rememberMe = this.driver.findElement(By.name("rememberMe"));

        //操作页面元素
        elemEmail.clear();
        elemPassword.clear();

        elemEmail.sendKeys("827358889@qq.com");
        //elemEmail.sendKeys("email");
        elemPassword.sendKeys("xx7220058");
        //elemPassword.sendKeys("password");
        rememberMe.click();
        btn.click();

        //提交表单
        btn.submit();

        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        this.driver.get("https://www.amazon.com/");

        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        //获取cookies
        //driver.manage().getCookies();
        Set<Cookie> cookies = this.driver.manage().getCookies();
        System.out.println("Size:" + cookies.size());

        Iterator<Cookie> itr = cookies.iterator();
        BasicCookieStore cookieStore = new BasicCookieStore();

        while (itr.hasNext()) {
            Cookie cookie = itr.next();
            BasicClientCookie bcco = new BasicClientCookie(cookie.getName(), cookie.getValue());
            bcco.setDomain(cookie.getDomain());
            bcco.setPath(cookie.getPath());
            cookieStore.addCookie(bcco);
        }
    }

    private WebDriver getDriver() {
        System.setProperty("webdriver.gecko.driver", "/media/peter/Data/dev/bisonSpace/springboot_selenium_demo/src/main/resources/static/geckodriver-v0.24.0-linux64/geckodriver");
        driver = new FirefoxDriver();
        return driver;
    }

    @Override
    public void testComment() {

        //判断当前运行系统
        if (OSinfo.isLinux()){
            //如果是Linux系统
            //初始化参数
            //System.setProperty("webdriver.gecko.driver", "/media/peter/Data/dev/bisonSpace/springboot_selenium_demo/src/main/resources/static/geckodriver-v0.24.0-linux64/geckodriver");
            driver = new FirefoxDriver();
        }else if (OSinfo.isWindows()){
            System.setProperty("webdriver.chrome.driver", "D:/F/workspace/ideaproject/springboot_selenium_demo/src/main/resources/static/chromedriver_win32/chromedriver.exe");
            driver = new ChromeDriver();
        }



        //需要comment的url
        String commentUrl = "https://www.amazon.com/gp/customer-reviews/RBUNP9DLDMGMY/ref=cm_cr_arp_d_rvw_ttl?ie=UTF8&ASIN=B07HVVHL23";

        //加载url
        this.driver.get(commentUrl);

        //等待加载完成
        this.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

        // 获取页面元素
        WebElement elemComment = this.driver.findElement(By.className("a-size-base"));
        WebElement elemCommentNum = this.driver.findElement(By.className("review-comment-total aok-hidden"));

        //判断是否已经有comment,以及是否是我们自己的comment
        if (elemCommentNum.getText().equals("0")) {
            //没有comment,直接查询模板comment
            //点击Comment
            elemComment.click();

            //填写comment**********************************************8
            //填写comment**********************************************8
            WebElement elemText = this.driver.findElement(By.className("a-button-text"));

            //提交
            elemText.click();
        } else {
            //判断是否有我们自己的comment
            elemComment.click();

            //判断已有的comment
            WebElement isOurComment = this.driver.findElement(By.className("review-comment-text"));
            String text = isOurComment.getText();

            if (text.contains("Seller Support")) {

                String nextUrl = "https://www.amazon.com/gp/customer-reviews/R1799KQY7RIVPZ/ref=cm_cr_arp_d_rvw_ttl?ie=UTF8&ASIN=B07H7G77X4";
                //下一个URL
                driver.get(nextUrl);
            } else {
                //填写comment**********************************************8
                WebElement elemComment1 = this.driver.findElement(By.className("a-button-input"));
                elemComment1.click();
                //填写comment**********************************************8

                WebElement elemText = this.driver.findElement(By.className("a-button-text"));

                //提交
                elemText.click();
            }
        }
    }


   /* public void getCookie1() {
        //初始化参数
        System.setProperty("webdriver.chrome.driver", "/media/peter/Data/dev/bisonSpace/springboot_selenium_demo/src/main/resources/static/chromedriver_linux64/chromedriver");
        WebDriver driver = new ChromeDriver();
        String baseUrl = "https://www.amazon.com/ap/signin?openid.pape.max_auth_age=0&openid.return_to=https%3A%2F%2Fwww.amazon.com%2F%3Fref_%3Dnav_ya_signin&openid.identity=http%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0%2Fidentifier_select&openid.assoc_handle=usflex&openid.mode=checkid_setup&openid.claimed_id=http%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0%2Fidentifier_select&openid.ns=http%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0&";
        //加载url
        driver.get(baseUrl);
        //等待加载完成
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

        // 获取页面元素
        WebElement elemEmail = driver.findElement(By.name("email"));
        WebElement elemPassword = driver.findElement(By.name("password"));
        // WebElement btn = driver.findElement(By.className("logging"));
        WebElement btn = driver.findElement(By.id("signInSubmit"));
        WebElement rememberMe = driver.findElement(By.name("rememberMe"));

        //操作页面元素
        elemEmail.clear();
        elemPassword.clear();

        elemEmail.sendKeys("email");
        elemPassword.sendKeys("password");
        rememberMe.click();
        btn.click();

        //提交表单
        btn.submit();

        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        driver.get("https://www.amazon.com/");

        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        //获取cookies
        //driver.manage().getCookies();
        Set<Cookie> cookies = driver.manage().getCookies();
        System.out.println("Size:" + cookies.size());

        Iterator<Cookie> itr = cookies.iterator();
        BasicCookieStore cookieStore = new BasicCookieStore();

        while (itr.hasNext()) {
            Cookie cookie = itr.next();
            BasicClientCookie bcco = new BasicClientCookie(cookie.getName(), cookie.getValue());
            bcco.setDomain(cookie.getDomain());
            bcco.setPath(cookie.getPath());
            cookieStore.addCookie(bcco);
        }


    }*/
}
