package com.example.springboot_selenium_demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootSeleniumDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootSeleniumDemoApplication.class, args);
    }

}
